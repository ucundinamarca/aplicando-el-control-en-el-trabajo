/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='images/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "both",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: 'screen1',
                            type: 'group',
                            rect: ['0', '0', '1024', '640', 'auto', 'auto'],
                            c: [
                            {
                                id: 'Recurso_1',
                                type: 'image',
                                rect: ['0px', '0px', '1024px', '640px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_1.png",'0px','0px']
                            },
                            {
                                id: 'Recurso_2',
                                type: 'image',
                                rect: ['80px', '108px', '864px', '212px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_2.png",'0px','0px']
                            },
                            {
                                id: 'btn_iniciar',
                                type: 'image',
                                rect: ['443px', '376px', '138px', '44px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"Recurso_3.png",'0px','0px']
                            },
                            {
                                id: 'Recurso_4',
                                type: 'image',
                                rect: ['393px', '582px', '226px', '38px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_4.png",'0px','0px']
                            },
                            {
                                id: 'Group3',
                                type: 'group',
                                rect: ['20', '14', '988', '40', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'Text',
                                    type: 'text',
                                    rect: ['0px', '0px', '283px', '28px', 'auto', 'auto'],
                                    text: "<p style=\"margin: 0px;\">​<span style=\"font-size: 15px;\">Curso Planeación, Seguimiento y Control &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</span></p>",
                                    font: ['Arial, Helvetica, sans-serif', [16, "px"], "rgba(0,0,0,1)", "normal", "none", "", "break-word", ""]
                                },
                                {
                                    id: 'TextCopy',
                                    type: 'text',
                                    rect: ['725px', '0px', '263px', '28px', 'auto', 'auto'],
                                    text: "<p style=\"margin: 0px;\">​<span style=\"font-size: 15px;\">&nbsp;RED Aplicando el control en el trabajo</span>&nbsp;</p>",
                                    font: ['Arial, Helvetica, sans-serif', [16, "px"], "rgba(0,0,0,1)", "normal", "none", "", "break-word", ""]
                                },
                                {
                                    id: 'Rectangle',
                                    type: 'rect',
                                    rect: ['0px', '37px', '980px', '3px', 'auto', 'auto'],
                                    fill: ["rgba(164,105,83,1.00)"],
                                    stroke: [0,"rgba(0,0,0,1)","none"]
                                }]
                            }]
                        },
                        {
                            id: 'screen2',
                            type: 'group',
                            rect: ['26', '87', '990', '394', 'auto', 'auto'],
                            c: [
                            {
                                id: 'matera',
                                type: 'image',
                                rect: ['894px', '222px', '96px', '172px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"matera.svg",'0px','0px']
                            },
                            {
                                id: 'reloj',
                                type: 'image',
                                rect: ['829px', '0px', '82px', '82px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"reloj.svg",'0px','0px']
                            },
                            {
                                id: 'estante',
                                type: 'image',
                                rect: ['0px', '28px', '186px', '136px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"estante.svg",'0px','0px']
                            }]
                        },
                        {
                            id: 'screen3',
                            type: 'group',
                            rect: ['241px', '141px', '593', '418', 'auto', 'auto'],
                            c: [
                            {
                                id: 'avatar',
                                type: 'group',
                                rect: ['-30', '28', '245', '390', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'avatar2',
                                    symbolName: 'avatar',
                                    type: 'rect',
                                    rect: ['0', '0', '245', '390', 'auto', 'auto']
                                }]
                            },
                            {
                                id: 'caja',
                                type: 'group',
                                rect: ['232', '0', '361', '383', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'box',
                                    type: 'group',
                                    rect: ['0px', '0px', '361', '383', 'auto', 'auto'],
                                    clip: 'rect(0px 361px 383px 0px)',
                                    c: [
                                    {
                                        id: 'Recurso_11',
                                        type: 'image',
                                        rect: ['0px', '0px', '361px', '383px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"Recurso_11.png",'0px','0px']
                                    },
                                    {
                                        id: 'Recurso_13',
                                        type: 'image',
                                        rect: ['295px', '334px', '41px', '39px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"Recurso_13.png",'0px','0px']
                                    },
                                    {
                                        id: 'Recurso_12',
                                        type: 'image',
                                        rect: ['19px', '334px', '35px', '39px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"Recurso_12.png",'0px','0px']
                                    },
                                    {
                                        id: 'Group6',
                                        type: 'group',
                                        rect: ['28', '28', '301', '271', 'auto', 'auto'],
                                        c: [
                                        {
                                            id: 't1',
                                            type: 'text',
                                            rect: ['0px', '0px', '301px', '271px', 'auto', 'auto'],
                                            text: "<p style=\"margin: 0px;\">​1. Lea y analice en detalle la etapa final del caso que venimos trabajando:</p><p style=\"margin: 0px;\">​</p><p style=\"margin: 0px;\">La funcionaria Leidy Duque debe establecer los indicadores de gestión correspondientes al plan de acción formulado previamente por ella bajo la estrategia denominada “Implementación de un sistema de planeación en el área de trabajo”, que se presenta a través de la siguiente tabla:</p>",
                                            align: "justify",
                                            userClass: "contenido",
                                            font: ['Arial, Helvetica, sans-serif', [16, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "normal"],
                                            textStyle: ["", "", "", "", "none"]
                                        },
                                        {
                                            id: 't2',
                                            type: 'text',
                                            rect: ['0px', '0px', '301px', '271px', 'auto', 'auto'],
                                            text: "<p style=\"margin: 0px;\">​2. El jefe de oficina le solicita a Leidy identificar tres (3) indicadores de gestión&nbsp;con el fin de medir el alcance de la meta propuesta. Leidy recurre a usted para que le brinde la asesoría necesaria, la cual se desarrollará en las dos (2) etapas de esta actividad. &nbsp;</p><p style=\"margin: 0px;\">​</p><p style=\"margin: 0px;\"></p>",
                                            align: "justify",
                                            userClass: "contenido",
                                            font: ['Arial, Helvetica, sans-serif', [16, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "normal"],
                                            textStyle: ["", "", "", "", "none"]
                                        },
                                        {
                                            id: 't3',
                                            type: 'group',
                                            rect: ['0px', '0px', '301', '271', 'auto', 'auto'],
                                            userClass: "contenido",
                                            c: [
                                            {
                                                id: 't3_',
                                                type: 'text',
                                                rect: ['0px', '0px', '301px', '65px', 'auto', 'auto'],
                                                text: "<p style=\"margin: 0px;\">​3. Para un efectivo análisis del caso, revise a profundidad los siguientes recursos de apoyo:</p><p style=\"margin: 0px;\">​</p><p style=\"margin: 0px;\">​</p><p style=\"margin: 0px;\"></p>",
                                                align: "justify",
                                                font: ['Arial, Helvetica, sans-serif', [16, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "normal"],
                                                textStyle: ["", "", "", "", "none"]
                                            },
                                            {
                                                id: 'link1',
                                                type: 'image',
                                                rect: ['7px', '71px', '25px', '26px', 'auto', 'auto'],
                                                fill: ["rgba(0,0,0,0)",im+"Recurso_17.png",'0px','0px']
                                            },
                                            {
                                                id: 'texto_link_1',
                                                type: 'text',
                                                rect: ['37px', '71px', '264px', '44px', 'auto', 'auto'],
                                                text: "<p style=\"margin: 0px;\">​Uribe Macías, M. E. y Reinoso Lastra, J. F. (2014). Sistema de indicadores de gestión. Recuperado de https://www-ebooks7-24-com.ucundinamarca.basesdedatosezproxy.com/stage.aspx?il=5578&amp;pg=&amp;ed=</p>",
                                                align: "left",
                                                font: ['Arial, Helvetica, sans-serif', [10, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "normal"],
                                                textStyle: ["", "", "10px", "", "none"]
                                            },
                                            {
                                                id: 'link2',
                                                type: 'image',
                                                rect: ['7px', '127px', '25px', '20px', 'auto', 'auto'],
                                                fill: ["rgba(0,0,0,0)",im+"Recurso_17.png",'0px','0px']
                                            },
                                            {
                                                id: 'texto_link_2',
                                                type: 'text',
                                                rect: ['37px', '127px', '264px', '42px', 'auto', 'auto'],
                                                text: "<p style=\"margin: 0px;\">​<span style=\"font-size: 12px;\"></span>Lezama Osaín, C. (s.f).  Indicadores de gestión (pp. 1-35). Recuperado de https://www.ucipfg.com/Repositorio/MLGA/MLGA-03/semana2/indicadores-de-gestion.pdf</p>",
                                                align: "left",
                                                font: ['Arial, Helvetica, sans-serif', [10, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "normal"],
                                                textStyle: ["", "", "10px", "", "none"]
                                            },
                                            {
                                                id: 'link3',
                                                type: 'image',
                                                rect: ['7px', '175px', '25px', '26px', 'auto', 'auto'],
                                                fill: ["rgba(0,0,0,0)",im+"Recurso_17.png",'0px','0px']
                                            },
                                            {
                                                id: 'texto_link_3',
                                                type: 'text',
                                                rect: ['37px', '175px', '264px', '42px', 'auto', 'auto'],
                                                text: "<p style=\"margin: 0px;\">​<span style=\"font-size: 12px;\"></span>Departamento Administrativo de la Función Pública (2012). Guía para la construcción de indicadores de gestión (pp. 11-34). Recuperado de  https://www.funcionpublica.gov.co/documents/418537/506911/1595.pdf/6c897f03-9b26-4e10-85a7-789c9e54f5a3&nbsp;</p>",
                                                align: "left",
                                                font: ['Arial, Helvetica, sans-serif', [10, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "normal"],
                                                textStyle: ["", "", "10px", "", "none"]
                                            },
                                            {
                                                id: 'link4',
                                                type: 'image',
                                                rect: ['7px', '236px', '25px', '26px', 'auto', 'auto'],
                                                fill: ["rgba(0,0,0,0)",im+"Recurso_17.png",'0px','0px']
                                            },
                                            {
                                                id: 'texto_link_4',
                                                type: 'text',
                                                rect: ['37px', '236px', '268px', '49px', 'auto', 'auto'],
                                                text: "<p style=\"margin: 0px;\">​<span style=\"font-size: 12px;\"></span>Rodríguez, M. (2014). Indicadores de gestión en la gerencia estratégica universitaria. Revista Científica Ciencias Humanas, 9(27), pp. 31-46. Recuperado de https://www.redalyc.org/pdf/709/70930407002.pdf.</p><p style=\"margin: 0px;\">​</p>",
                                                align: "left",
                                                font: ['Arial, Helvetica, sans-serif', [10, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "normal"],
                                                textStyle: ["", "", "10px", "", "none"]
                                            }]
                                        },
                                        {
                                            id: 't4',
                                            type: 'text',
                                            rect: ['0px', '0px', '301px', '271px', 'auto', 'auto'],
                                            text: "<p style=\"margin: 0px;\">​<span style=\"font-size: 14px;\">4. Una vez analizado el caso a partir de la consulta de los recursos de apoyo sugeridos, seleccione entre las distintas opciones que encuentra a continuación <span id='r'>los tres (3) indicadores de gestión</span>  que usted considere se aplican al caso presentado y que servirán para medir el alcance de la meta definida en el plan de acción de la funcionaria Leidy Duque.&nbsp;</span></p><p style=\"margin: 0px;\"><span style=\"font-size: 14px;\">​</span></p><p style=\"margin: 0px;\"><span style=\"font-size: 14px;\">Cuenta con dos (2) intentos para realizar la selección de los tres (3) indicadores correctos. Cada indicador de gestión acertado equivaldrá a un (1) punto conseguido&nbsp;y cada desacierto a cero (0) puntos. Total de puntos a obtener en la etapa 1 de su asesoría: tres (3) puntos.</span></p><p style=\"margin: 0px;\"><span style=\"font-size: 14px;\">&nbsp;</span></p><p style=\"margin: 0px;\"><span style=\"font-size: 14px;\">​</span></p><p style=\"margin:0px\"><span style=\"font-size: 14px;\">​</span></p>",
                                            align: "justify",
                                            userClass: "contenido",
                                            font: ['Arial, Helvetica, sans-serif', [14, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "normal"],
                                            textStyle: ["", "", "", "", "none"]
                                        },
                                        {
                                            id: 't5',
                                            type: 'text',
                                            rect: ['0px', '0px', '301px', '271px', 'auto', 'auto'],
                                            text: "<p style=\"margin: 0px;\">​<span style=\"font-size: 14px;\"></span>5. Teniendo en cuenta los tres (3) indicadores de gestión aplicables al caso presentado, relaciónelos a continuación con sus respectivos objetos de medición. Para ello, arrastre cada caja de texto de la columna B hacia la opción correspondiente de la columna A hasta completar la tabla.&nbsp;</p><p style=\"margin: 0px;\">​</p><p style=\"margin: 0px;\">Cuenta con dos (2) intentos para relacionar correctamente los objetos de medición con los indicadores de gestión correspondientes. Cada objeto de medición relacionado acertadamente con su indicador de gestión respetivo equivaldrá a un (1) punto conseguido&nbsp;y cada desacierto a cero (0) puntos. Total de puntos a obtener en la etapa 2 de su asesoría: tres (3) puntos.</p><p style=\"margin: 0px;\"><span style=\"font-size: 14px;\"></span></p>",
                                            align: "justify",
                                            userClass: "contenido",
                                            font: ['Arial, Helvetica, sans-serif', [14, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "normal"],
                                            textStyle: ["", "", "", "", "none"]
                                        }]
                                    }]
                                },
                                {
                                    id: 'Text7',
                                    type: 'text',
                                    rect: ['146px', '344px', 'auto', 'auto', 'auto', 'auto'],
                                    cursor: 'pointer',
                                    text: "<p style=\"margin: 0px;\">​Ver Tabla</p>",
                                    align: "justify",
                                    font: ['Arial, Helvetica, sans-serif', [14, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "nowrap"],
                                    textStyle: ["", "", "", "", "none"]
                                }]
                            },
                            {
                                id: 'indicadores',
                                type: 'group',
                                rect: ['152', '2px', '583', '383', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'Recurso_18',
                                    type: 'image',
                                    rect: ['0px', '0px', '583px', '383px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_18.png",'0px','0px']
                                },
                                {
                                    id: 'Group2',
                                    type: 'group',
                                    rect: ['20', '29', '545', '337', 'auto', 'auto'],
                                    c: [
                                    {
                                        id: 'btn1',
                                        type: 'group',
                                        rect: ['3px', '0px', '168', '62', 'auto', 'auto'],
                                        cursor: 'pointer',
                                        userClass: "btn",
                                        c: [
                                        {
                                            id: 'f1',
                                            type: 'image',
                                            rect: ['0px', '0px', '168px', '62px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Recurso_20.png",'0px','0px']
                                        },
                                        {
                                            id: 'f2',
                                            type: 'image',
                                            rect: ['0px', '0px', '167px', '62px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Recurso_19.png",'0px','0px'],
                                            userClass: "btn_aux"
                                        },
                                        {
                                            id: 't_btn1',
                                            type: 'text',
                                            rect: ['8px', '13', '153px', '32px', 'auto', 'auto'],
                                            text: "<p style=\"margin: 0px; text-align: center;\">​Efectividad&nbsp;</p><p style=\"margin: 0px; text-align: center;\">​comercial</p>",
                                            align: "justify",
                                            userClass: "texto",
                                            font: ['Arial, Helvetica, sans-serif', [14, "px"], "rgba(0,0,0,1)", "700", "none", "normal", "break-word", ""],
                                            textStyle: ["", "", "", "", "none"]
                                        }]
                                    },
                                    {
                                        id: 'btn2',
                                        type: 'group',
                                        rect: ['190px', '0px', '168', '62', 'auto', 'auto'],
                                        cursor: 'pointer',
                                        userClass: "btn",
                                        c: [
                                        {
                                            id: 'f1Copy',
                                            type: 'image',
                                            rect: ['0px', '0px', '168px', '62px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Recurso_20.png",'0px','0px']
                                        },
                                        {
                                            id: 'f2Copy',
                                            type: 'image',
                                            rect: ['0px', '0px', '167px', '62px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Recurso_19.png",'0px','0px'],
                                            userClass: "btn_aux"
                                        },
                                        {
                                            id: 't_btn1Copy',
                                            type: 'text',
                                            rect: ['8px', '13', '153px', '32px', 'auto', 'auto'],
                                            text: "<p style=\"margin: 0px; text-align: center;\">​N.º de reuniones para ajustar planes</p><p style=\"margin: 0px; text-align: center;\"></p>",
                                            align: "justify",
                                            userClass: "texto",
                                            font: ['Arial, Helvetica, sans-serif', [14, "px"], "rgba(0,0,0,1)", "700", "none", "normal", "break-word", ""],
                                            textStyle: ["", "", "", "", "none"]
                                        }]
                                    },
                                    {
                                        id: 'btn3',
                                        type: 'group',
                                        rect: ['377px', '0px', '168', '62', 'auto', 'auto'],
                                        cursor: 'pointer',
                                        userClass: "btn",
                                        c: [
                                        {
                                            id: 'f1Copy2',
                                            type: 'image',
                                            rect: ['0px', '0px', '168px', '62px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Recurso_20.png",'0px','0px']
                                        },
                                        {
                                            id: 'f2Copy2',
                                            type: 'image',
                                            rect: ['0px', '0px', '167px', '62px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Recurso_19.png",'0px','0px'],
                                            userClass: "btn_aux"
                                        },
                                        {
                                            id: 't_btn1Copy2',
                                            type: 'text',
                                            rect: ['8px', '12px', '153px', '32px', 'auto', 'auto'],
                                            text: "<p style=\"margin: 0px; text-align: center;\">​Adquisición de maquinaria y equipo a departamentos de la organización<span style=\"font-size: 10px;\">&nbsp;</span></p><p style=\"margin: 0px; text-align: center;\"></p>",
                                            align: "justify",
                                            userClass: "texto",
                                            font: ['Arial, Helvetica, sans-serif', [11, "px"], "rgba(0,0,0,1)", "700", "none", "normal", "break-word", ""],
                                            textStyle: ["", "", "", "", "none"]
                                        }]
                                    },
                                    {
                                        id: 'btn4',
                                        type: 'group',
                                        rect: ['3px', '87px', '168', '62', 'auto', 'auto'],
                                        cursor: 'pointer',
                                        userClass: "btn",
                                        c: [
                                        {
                                            id: 'f1Copy5',
                                            type: 'image',
                                            rect: ['0px', '0px', '168px', '62px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Recurso_20.png",'0px','0px']
                                        },
                                        {
                                            id: 'f2Copy5',
                                            type: 'image',
                                            rect: ['0px', '0px', '167px', '62px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Recurso_19.png",'0px','0px'],
                                            userClass: "btn_aux"
                                        },
                                        {
                                            id: 't_btn1Copy5',
                                            type: 'text',
                                            rect: ['8px', '7px', '153px', '32px', 'auto', 'auto'],
                                            text: "<p style=\"margin: 0px; text-align: center;\">​Reparación oportuna de equipos que fallen en la organización&nbsp;</p><p style=\"margin: 0px; text-align: center;\"></p>",
                                            align: "justify",
                                            userClass: "texto",
                                            font: ['Arial, Helvetica, sans-serif', [14, "px"], "rgba(0,0,0,1)", "700", "none", "normal", "break-word", ""],
                                            textStyle: ["", "", "", "", "none"]
                                        }]
                                    },
                                    {
                                        id: 'btn5',
                                        type: 'group',
                                        rect: ['190px', '87px', '168', '62', 'auto', 'auto'],
                                        cursor: 'pointer',
                                        userClass: "btn",
                                        c: [
                                        {
                                            id: 'f1Copy4',
                                            type: 'image',
                                            rect: ['0px', '0px', '168px', '62px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Recurso_20.png",'0px','0px']
                                        },
                                        {
                                            id: 'f2Copy4',
                                            type: 'image',
                                            rect: ['0px', '0px', '167px', '62px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Recurso_19.png",'0px','0px'],
                                            userClass: "btn_aux"
                                        },
                                        {
                                            id: 't_btn1Copy4',
                                            type: 'text',
                                            rect: ['8px', '7px', '153px', '32px', 'auto', 'auto'],
                                            text: "<p style=\"margin: 0px; text-align: center;\">​Tiempo improductivo en prestación del servicio</p><p style=\"margin: 0px; text-align: center;\"></p>",
                                            align: "justify",
                                            userClass: "texto",
                                            font: ['Arial, Helvetica, sans-serif', [14, "px"], "rgba(0,0,0,1)", "700", "none", "normal", "break-word", ""],
                                            textStyle: ["", "", "", "", "none"]
                                        }]
                                    },
                                    {
                                        id: 'btn6',
                                        type: 'group',
                                        rect: ['377px', '87px', '168', '62', 'auto', 'auto'],
                                        cursor: 'pointer',
                                        userClass: "btn",
                                        c: [
                                        {
                                            id: 'f1Copy3',
                                            type: 'image',
                                            rect: ['0px', '0px', '168px', '62px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Recurso_20.png",'0px','0px']
                                        },
                                        {
                                            id: 'f2Copy3',
                                            type: 'image',
                                            rect: ['0px', '0px', '167px', '62px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Recurso_19.png",'0px','0px'],
                                            userClass: "btn_aux"
                                        },
                                        {
                                            id: 't_btn1Copy3',
                                            type: 'text',
                                            rect: ['8px', '5px', '153px', '32px', 'auto', 'auto'],
                                            text: "<p style=\"margin: 0px; text-align: center;\">​N.º de planes recibidos por las dependencias</p><p style=\"margin: 0px; text-align: center;\"></p>",
                                            align: "justify",
                                            userClass: "texto",
                                            font: ['Arial, Helvetica, sans-serif', [14, "px"], "rgba(0,0,0,1)", "700", "none", "normal", "break-word", ""],
                                            textStyle: ["", "", "", "", "none"]
                                        }]
                                    },
                                    {
                                        id: 'btn7',
                                        type: 'group',
                                        rect: ['3px', '173px', '168', '62', 'auto', 'auto'],
                                        cursor: 'pointer',
                                        userClass: "btn",
                                        c: [
                                        {
                                            id: 'f1Copy8',
                                            type: 'image',
                                            rect: ['0px', '0px', '168px', '62px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Recurso_20.png",'0px','0px']
                                        },
                                        {
                                            id: 'f2Copy8',
                                            type: 'image',
                                            rect: ['0px', '0px', '167px', '62px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Recurso_19.png",'0px','0px'],
                                            userClass: "btn_aux"
                                        },
                                        {
                                            id: 't_btn1Copy8',
                                            type: 'text',
                                            rect: ['8px', '13', '153px', '32px', 'auto', 'auto'],
                                            text: "<p style=\"margin: 0px; text-align: center;\">​N.º de planes de trabajo diseñados</p><p style=\"margin: 0px; text-align: center;\"></p>",
                                            align: "justify",
                                            userClass: "texto",
                                            font: ['Arial, Helvetica, sans-serif', [14, "px"], "rgba(0,0,0,1)", "700", "none", "normal", "break-word", ""],
                                            textStyle: ["", "", "", "", "none"]
                                        }]
                                    },
                                    {
                                        id: 'btn8',
                                        type: 'group',
                                        rect: ['190px', '173px', '168', '62', 'auto', 'auto'],
                                        cursor: 'pointer',
                                        userClass: "btn",
                                        c: [
                                        {
                                            id: 'f1Copy7',
                                            type: 'image',
                                            rect: ['0px', '0px', '168px', '62px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Recurso_20.png",'0px','0px']
                                        },
                                        {
                                            id: 'f2Copy7',
                                            type: 'image',
                                            rect: ['0px', '0px', '167px', '62px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Recurso_19.png",'0px','0px'],
                                            userClass: "btn_aux"
                                        },
                                        {
                                            id: 't_btn1Copy7',
                                            type: 'text',
                                            rect: ['8px', '13', '153px', '32px', 'auto', 'auto'],
                                            text: "<p style=\"margin: 0px; text-align: center;\">​Ausentismo de personal</p><p style=\"margin: 0px; text-align: center;\"></p>",
                                            align: "justify",
                                            userClass: "texto",
                                            font: ['Arial, Helvetica, sans-serif', [14, "px"], "rgba(0,0,0,1)", "700", "none", "normal", "break-word", ""],
                                            textStyle: ["", "", "", "", "none"]
                                        }]
                                    },
                                    {
                                        id: 'btn9',
                                        type: 'group',
                                        rect: ['377px', '173px', '168', '62', 'auto', 'auto'],
                                        cursor: 'pointer',
                                        userClass: "btn",
                                        c: [
                                        {
                                            id: 'f1Copy6',
                                            type: 'image',
                                            rect: ['0px', '0px', '168px', '62px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Recurso_20.png",'0px','0px']
                                        },
                                        {
                                            id: 'f2Copy6',
                                            type: 'image',
                                            rect: ['0px', '0px', '167px', '62px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Recurso_19.png",'0px','0px'],
                                            userClass: "btn_aux"
                                        },
                                        {
                                            id: 't_btn1Copy6',
                                            type: 'text',
                                            rect: ['8px', '22px', '153px', '24px', 'auto', 'auto'],
                                            text: "<p style=\"margin: 0px; text-align: center;\">​Endeudamiento total</p><p style=\"margin: 0px; text-align: center;\"></p>",
                                            align: "justify",
                                            userClass: "texto",
                                            font: ['Arial, Helvetica, sans-serif', [14, "px"], "rgba(0,0,0,1)", "700", "none", "normal", "break-word", ""],
                                            textStyle: ["", "", "", "", "none"]
                                        }]
                                    },
                                    {
                                        id: 'Group',
                                        type: 'group',
                                        rect: ['0px', '276px', '537', '61', 'auto', 'auto'],
                                        c: [
                                        {
                                            id: 'Recurso_21',
                                            type: 'image',
                                            rect: ['0px', '0px', '537px', '61px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Recurso_21.png",'0px','0px']
                                        },
                                        {
                                            id: 'Text4',
                                            type: 'text',
                                            rect: ['13px', '12px', '27px', '41px', 'auto', 'auto'],
                                            text: "<p style=\"margin: 0px;\">​<span style=\"font-size: 35px;\">3</span></p>",
                                            font: ['Arial, Helvetica, sans-serif', [28, "px"], "rgba(232,71,68,1.00)", "normal", "none", "", "break-word", "normal"]
                                        },
                                        {
                                            id: 'Text5',
                                            type: 'text',
                                            rect: ['40px', '18px', '39px', '11px', 'auto', 'auto'],
                                            text: "<p style=\"margin: 0px;\">​Puntos</p>",
                                            align: "left",
                                            font: ['Arial, Helvetica, sans-serif', [10, "px"], "rgba(232,71,68,1)", "400", "none", "normal", "break-word", ""],
                                            textStyle: ["", "", "", "", "none"]
                                        },
                                        {
                                            id: 'Text5Copy',
                                            type: 'text',
                                            rect: ['101px', '18px', '414px', '29px', 'auto', 'auto'],
                                            text: "<p style=\"margin: 0px;\">​Algo ha fallado. Recuerde la importancia de consultar los recursos de apoyo sugeridos en torno al tema “Indicadores de gestión”</p><p style=\"margin: 0px;\">​</p>",
                                            align: "justify",
                                            font: ['Arial, Helvetica, sans-serif', [10, "px"], "rgba(39,38,38,1.00)", "400", "none", "normal", "break-word", ""],
                                            textStyle: ["", "", "", "", "none"]
                                        }]
                                    }]
                                },
                                {
                                    id: 'Text11',
                                    type: 'text',
                                    rect: ['23px', '278px', '529px', '27px', 'auto', 'auto'],
                                    text: "<p style=\"margin: 0px;\">​</p>",
                                    align: "left",
                                    font: ['Arial, Helvetica, sans-serif', [10, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "normal"],
                                    textStyle: ["", "", "", "", "none"]
                                },
                                {
                                    id: 'Group4',
                                    type: 'group',
                                    rect: ['12px', '44px', '539', '324', 'auto', 'auto'],
                                    c: [
                                    {
                                        id: 'r3',
                                        type: 'image',
                                        rect: ['110px', '223px', '183px', '95px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"Recurso_23.png",'0px','0px'],
                                        userClass: "droppable"
                                    },
                                    {
                                        id: 'r2',
                                        type: 'image',
                                        rect: ['110px', '114px', '183px', '95px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"Recurso_23.png",'0px','0px'],
                                        userClass: "droppable"
                                    },
                                    {
                                        id: 'r1',
                                        type: 'image',
                                        rect: ['110px', '2px', '183px', '95px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"Recurso_23.png",'0px','0px'],
                                        userClass: "droppable"
                                    },
                                    {
                                        id: 'legend',
                                        type: 'text',
                                        rect: ['344px', '5px', '207px', '73px', 'auto', 'auto'],
                                        text: "<p style=\"margin: 0px;\">​</p>",
                                        font: ['Arial, Helvetica, sans-serif', [10, "px"], "rgba(0,0,0,1)", "normal", "none", "", "break-word", "normal"]
                                    },
                                    {
                                        id: 'c3',
                                        type: 'group',
                                        rect: ['0px', '222px', '102', '94', 'auto', 'auto'],
                                        c: [
                                        {
                                            id: 'ff3',
                                            type: 'image',
                                            rect: ['0px', '0px', '102px', '94px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Recurso_24.png",'0px','0px']
                                        },
                                        {
                                            id: 'Text8Copy2',
                                            type: 'text',
                                            rect: ['11px', '25px', '76px', '42px', 'auto', 'auto'],
                                            text: "<p style=\"margin: 0px;\">​N.º de reuniones para ajustar planes</p>",
                                            align: "center",
                                            font: ['Arial, Helvetica, sans-serif', [10, "px"], "rgba(255,255,255,1.00)", "normal", "none", "", "break-word", "normal"]
                                        }]
                                    },
                                    {
                                        id: 'c2',
                                        type: 'group',
                                        rect: ['0px', '112px', '102', '94', 'auto', 'auto'],
                                        c: [
                                        {
                                            id: 'ff2',
                                            type: 'image',
                                            rect: ['0px', '0px', '102px', '94px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Recurso_24.png",'0px','0px']
                                        },
                                        {
                                            id: 'Text8Copy',
                                            type: 'text',
                                            rect: ['11px', '26px', '76px', '42px', 'auto', 'auto'],
                                            text: "<p style=\"margin: 0px;\">​N.º de planes recibidos por las dependencias</p><p style=\"margin: 0px;\">​</p>",
                                            align: "center",
                                            font: ['Arial, Helvetica, sans-serif', [10, "px"], "rgba(255,255,255,1.00)", "normal", "none", "", "break-word", "normal"]
                                        }]
                                    },
                                    {
                                        id: 'c1',
                                        type: 'group',
                                        rect: ['0px', '3px', '102', '94', 'auto', 'auto'],
                                        c: [
                                        {
                                            id: 'ff1',
                                            type: 'image',
                                            rect: ['0px', '0px', '102px', '94px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Recurso_24.png",'0px','0px']
                                        },
                                        {
                                            id: 'Text8',
                                            type: 'text',
                                            rect: ['11px', '24px', '76px', '42px', 'auto', 'auto'],
                                            text: "<p style=\"margin: 0px;\">​N.º de planes de trabajo diseñados</p><p style=\"margin: 0px;\">​</p>",
                                            align: "center",
                                            font: ['Arial, Helvetica, sans-serif', [10, "px"], "rgba(255,255,255,1.00)", "normal", "none", "", "break-word", "normal"]
                                        }]
                                    },
                                    {
                                        id: 'g1',
                                        type: 'group',
                                        rect: ['344px', '0px', '216', '101', 'auto', 'auto'],
                                        userClass: "draggable",
                                        c: [
                                        {
                                            id: 'Recurso_25',
                                            type: 'image',
                                            rect: ['0px', '0px', '216px', '101px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Recurso_25.png",'0px','0px']
                                        },
                                        {
                                            id: 'Text8Copy3',
                                            type: 'text',
                                            rect: ['11px', '15px', '198px', '71px', 'auto', 'auto'],
                                            text: "<p style=\"margin: 0px;\">​Este indicador tiene como contexto los objetivos y metas a alcanzar en el plan de acción, en el que cada departamento o área contribuye y orienta a sus actividades a dicho propósito.</p><p style=\"margin: 0px;\">​</p>",
                                            align: "left",
                                            font: ['Arial, Helvetica, sans-serif', [10, "px"], "rgba(255,255,255,1.00)", "normal", "none", "", "break-word", "normal"]
                                        }]
                                    },
                                    {
                                        id: 'g2',
                                        type: 'group',
                                        rect: ['344px', '112px', '216', '101', 'auto', 'auto'],
                                        userClass: "draggable",
                                        c: [
                                        {
                                            id: 'Recurso_25Copy',
                                            type: 'image',
                                            rect: ['0px', '0px', '216px', '101px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Recurso_25.png",'0px','0px']
                                        },
                                        {
                                            id: 'Text8Copy4',
                                            type: 'text',
                                            rect: ['11px', '11px', '198px', '71px', 'auto', 'auto'],
                                            text: "<p style=\"margin: 0px;\">​Este indicador tiene como contexto que cuando se pone en ejecución el plan de acción, éste debe monitorearse con la finalidad de establecer en qué punto se ubica y determinar qué modificaciones se requieren.</p><p style=\"margin: 0px;\"></p>",
                                            align: "left",
                                            font: ['Arial, Helvetica, sans-serif', [10, "px"], "rgba(255,255,255,1.00)", "normal", "none", "", "break-word", "normal"]
                                        }]
                                    },
                                    {
                                        id: 'g3',
                                        type: 'group',
                                        rect: ['344px', '223px', '216', '101', 'auto', 'auto'],
                                        userClass: "draggable",
                                        c: [
                                        {
                                            id: 'Recurso_25Copy2',
                                            type: 'image',
                                            rect: ['0px', '0px', '216px', '101px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Recurso_25.png",'0px','0px']
                                        },
                                        {
                                            id: 'Text8Copy5',
                                            type: 'text',
                                            rect: ['11px', '29px', '198px', '71px', 'auto', 'auto'],
                                            text: "<p style=\"margin: 0px;\">​Este indicador permite medir la cantidad de planes de acción establecidos en cada uno de los puestos de trabajo.</p><p style=\"margin: 0px;\">​</p>",
                                            align: "left",
                                            font: ['Arial, Helvetica, sans-serif', [10, "px"], "rgba(255,255,255,1.00)", "normal", "none", "", "break-word", "normal"]
                                        }]
                                    },
                                    {
                                        id: 'Text9',
                                        type: 'text',
                                        rect: ['0px', '-36px', '291px', '20px', 'auto', 'auto'],
                                        text: "<p style=\"margin: 0px;\">​Columna A </p><p style=\"margin: 0px;\">Indicadores de gestión</p>",
                                        align: "center",
                                        font: ['Arial, Helvetica, sans-serif', [14, "px"], "rgba(0,0,0,1)", "normal", "none", "", "break-word", "normal"]
                                    },
                                    {
                                        id: 'Text9Copy',
                                        type: 'text',
                                        rect: ['285px', '-36px', '291px', '20px', 'auto', 'auto'],
                                        text: "<p style=\"margin: 0px;\">​Columna B </p><p style=\"margin: 0px;\">Objetos de medición</p>",
                                        align: "center",
                                        font: ['Arial, Helvetica, sans-serif', [14, "px"], "rgba(0,0,0,1)", "normal", "none", "", "break-word", "normal"]
                                    },
                                    {
                                        id: 'Text12_',
                                        type: 'group',
                                        rect: ['-254px', '351', '537', '61', 'auto', 'auto'],
                                        c: [
                                        {
                                            id: 'Recurso_21Copy',
                                            type: 'image',
                                            rect: ['0px', '0px', '537px', '61px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Recurso_21.png",'0px','0px']
                                        },
                                        {
                                            id: 'Text12',
                                            type: 'text',
                                            rect: ['27px', '17px', '477px', '28px', 'auto', 'auto'],
                                            text: "<p style=\"margin: 0px;\">​</p>",
                                            align: "justify",
                                            font: ['Arial, Helvetica, sans-serif', [14, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "normal"],
                                            textStyle: ["", "", "", "", "none"]
                                        }]
                                    }]
                                },
                                {
                                    id: 'btn_sig',
                                    type: 'image',
                                    rect: ['442px', '396px', '138px', '44px', 'auto', 'auto'],
                                    cursor: 'pointer',
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_7.png",'0px','0px']
                                },
                                {
                                    id: 'reload1',
                                    type: 'group',
                                    rect: ['303px', '395', '98', '67', 'auto', 'auto'],
                                    cursor: 'pointer',
                                    c: [
                                    {
                                        id: 'Recurso_22',
                                        type: 'image',
                                        rect: ['26px', '0px', '50px', '50px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"Recurso_22.png",'0px','0px']
                                    },
                                    {
                                        id: 'Text6',
                                        type: 'text',
                                        rect: ['0px', '54px', 'auto', 'auto', 'auto', 'auto'],
                                        text: "<p style=\"margin: 0px;\">​<span style=\"color: rgb(255, 255, 255);\">Intentar nuevamente</span></p>",
                                        align: "justify",
                                        font: ['Arial, Helvetica, sans-serif', [10, "px"], "rgba(39,38,38,1)", "400", "none", "normal", "break-word", "nowrap"],
                                        textStyle: ["", "", "", "", "none"]
                                    }]
                                }]
                            }]
                        },
                        {
                            id: 'screen4',
                            type: 'group',
                            rect: ['241px', '141px', '593', '418', 'auto', 'auto'],
                            c: [
                            {
                                id: 'avatarCopy',
                                type: 'group',
                                rect: ['-30', '28', '245', '390', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'avatar3',
                                    symbolName: 'avatar',
                                    type: 'rect',
                                    rect: ['0', '0', '245', '390', 'auto', 'auto']
                                }]
                            },
                            {
                                id: 'caja2',
                                type: 'group',
                                rect: ['232', '0', '361', '383', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'Recurso_11Copy2',
                                    type: 'image',
                                    rect: ['0px', '0px', '361px', '383px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_11.png",'0px','0px']
                                }]
                            },
                            {
                                id: 'Text10',
                                type: 'text',
                                rect: ['256px', '26px', '316px', '156px', 'auto', 'auto'],
                                text: "<p style=\"margin: 0px;\">​</p>",
                                align: "justify",
                                font: ['Arial, Helvetica, sans-serif', [14, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "normal"],
                                textStyle: ["", "", "", "", "none"]
                            }]
                        },
                        {
                            id: 'msg',
                            type: 'group',
                            rect: ['0px', '1px', '1029px', '639px', 'auto', 'auto'],
                            c: [
                            {
                                id: 'Recurso_8',
                                type: 'image',
                                rect: ['190px', '160px', '662px', '334px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_8.png",'0px','0px']
                            },
                            {
                                id: 'Recurso_9',
                                type: 'image',
                                rect: ['227px', '252px', '600px', '1px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_9.png",'0px','0px']
                            },
                            {
                                id: 'Text3',
                                type: 'text',
                                rect: ['227px', '280px', '600px', '92px', 'auto', 'auto'],
                                text: "<p style=\"margin: 0px;\">​<span style=\"color: rgb(30, 30, 30);\">Identificar los indicadores de gestión que permiten evaluar y medir las metas establecidas en un plan de acción.</span></p>",
                                align: "center",
                                font: ['Arial, Helvetica, sans-serif', [24, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "normal"],
                                textStyle: ["", "", "", "", "none"]
                            },
                            {
                                id: 'Text2',
                                type: 'text',
                                rect: ['227px', '185px', '600px', '48px', 'auto', 'auto'],
                                text: "<p style=\"margin: 0px;\">​<span style=\"font-size: 42px;\">Objetivo</span></p>",
                                align: "center",
                                font: ['Arial, Helvetica, sans-serif', [24, ""], "rgba(0,0,0,1)", "normal", "none", "", "break-word", ""]
                            },
                            {
                                id: 'close',
                                type: 'text',
                                rect: ['825px', '158px', 'auto', 'auto', 'auto', 'auto'],
                                cursor: 'pointer',
                                text: "<p style=\"margin: 0px;\">​<span style=\"font-weight: 700;\">x</span></p>",
                                align: "center",
                                font: ['Arial, Helvetica, sans-serif', [30, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "nowrap"],
                                textStyle: ["", "", "", "", "none"]
                            }]
                        },
                        {
                            id: 'msg2',
                            type: 'group',
                            rect: ['0px', '1px', '1029px', '639px', 'auto', 'auto'],
                            c: [
                            {
                                id: 'Recurso_8Copy',
                                type: 'image',
                                rect: ['190px', '160px', '662px', '334px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_8.png",'0px','0px']
                            },
                            {
                                id: 'tt',
                                type: 'text',
                                rect: ['216px', '179px', '611px', '274px', 'auto', 'auto'],
                                text: "<p style=\"margin: 0px;\">​</p>",
                                align: "justify",
                                font: ['Arial, Helvetica, sans-serif', [18, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "normal"],
                                textStyle: ["", "", "", "", "none"]
                            },
                            {
                                id: 'close2',
                                type: 'text',
                                rect: ['825px', '158px', 'auto', 'auto', 'auto', 'auto'],
                                cursor: 'pointer',
                                text: "<p style=\"margin: 0px;\">​<span style=\"font-weight: 700;\">x</span></p>",
                                align: "center",
                                font: ['Arial, Helvetica, sans-serif', [30, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "nowrap"],
                                textStyle: ["", "", "", "", "none"]
                            }]
                        },
                        {
                            id: 'btn_creditos',
                            type: 'image',
                            rect: ['11px', '563px', '63px', '67px', 'auto', 'auto'],
                            cursor: 'pointer',
                            fill: ["rgba(0,0,0,0)",im+"Recurso_6.png",'0px','0px']
                        },
                        {
                            id: 'btn_objetivos',
                            type: 'image',
                            rect: ['83px', '563px', '63px', '67px', 'auto', 'auto'],
                            cursor: 'pointer',
                            fill: ["rgba(0,0,0,0)",im+"Recurso_10.png",'0px','0px']
                        },
                        {
                            id: 'creditos',
                            type: 'group',
                            rect: ['0px', '0px', '1024px', '640px', 'auto', 'auto'],
                            c: [
                            {
                                id: 'Recurso_28',
                                type: 'image',
                                rect: ['109px', '45px', '806px', '550px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"Recurso_28.png",'0px','0px']
                            }]
                        },
                        {
                            id: 'tablita',
                            type: 'group',
                            rect: ['2', '0', '1024', '640', 'auto', 'auto'],
                            c: [
                            {
                                id: 'Rectangle2',
                                type: 'rect',
                                rect: ['0px', '0px', '1024px', '640px', 'auto', 'auto'],
                                fill: ["rgba(39,45,65,0.54)"],
                                stroke: [0,"rgba(0,0,0,1)","none"]
                            },
                            {
                                id: 'table',
                                type: 'group',
                                rect: ['214', '16', '623', '606', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'tabla_',
                                    type: 'image',
                                    rect: ['0px', '0px', '623px', '606px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"tabla.png",'0px','0px']
                                },
                                {
                                    id: 'par',
                                    symbolName: 'par',
                                    type: 'rect',
                                    rect: ['12px', '85px', '600', '36', 'auto', 'auto']
                                }]
                            }]
                        },
                        {
                            id: 'preload',
                            type: 'group',
                            rect: ['0', '1', '1024', '640', 'auto', 'auto'],
                            c: [
                            {
                                id: 'Rectangle3',
                                type: 'rect',
                                rect: ['0px', '0px', '1024px', '640px', 'auto', 'auto'],
                                fill: ["rgba(255,255,255,1.00)"],
                                stroke: [0,"rgba(0,0,0,1)","none"]
                            },
                            {
                                id: 'Free-Loading-GIF-Icons',
                                type: 'image',
                                rect: ['327px', '168px', '400px', '300px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Free-Loading-GIF-Icons.gif",'0px','0px']
                            }]
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: ['null', 'null', '1024px', '640px', 'auto', 'auto'],
                            overflow: 'hidden',
                            fill: ["rgba(255,255,255,1)"]
                        }
                    }
                },
                timeline: {
                    duration: 6000,
                    autoPlay: true,
                    data: [

                    ]
                }
            },
            "avatar": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            rect: ['0px', '0px', '245px', '390px', 'auto', 'auto'],
                            id: 'Recurso_14',
                            type: 'image',
                            display: 'block',
                            fill: ['rgba(0,0,0,0)', 'images/Recurso_14.png', '0px', '0px']
                        },
                        {
                            rect: ['0px', '0px', '208px', '390px', 'auto', 'auto'],
                            id: 'Recurso_16',
                            type: 'image',
                            display: 'none',
                            fill: ['rgba(0,0,0,0)', 'images/Recurso_16.png', '0px', '0px']
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            rect: [null, null, '245px', '390px']
                        }
                    }
                },
                timeline: {
                    duration: 6000,
                    autoPlay: true,
                    data: [
                        [
                            "eid1",
                            "display",
                            0,
                            0,
                            "linear",
                            "${Recurso_16}",
                            'none',
                            'none'
                        ],
                        [
                            "eid2",
                            "display",
                            3000,
                            0,
                            "linear",
                            "${Recurso_16}",
                            'none',
                            'block'
                        ],
                        [
                            "eid3",
                            "display",
                            3000,
                            0,
                            "linear",
                            "${Recurso_14}",
                            'block',
                            'none'
                        ]
                    ]
                }
            },
            "par": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            type: 'rect',
                            id: 'Rectangle4',
                            stroke: [0, 'rgb(0, 0, 0)', 'none'],
                            rect: ['0px', '0px', '600px', '36px', 'auto', 'auto'],
                            fill: ['rgba(255,245,106,0.4706)']
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            rect: [null, null, '600px', '36px']
                        }
                    }
                },
                timeline: {
                    duration: 2000,
                    autoPlay: true,
                    data: [
                        [
                            "eid5",
                            "background-color",
                            0,
                            1000,
                            "linear",
                            "${Rectangle4}",
                            'rgba(255,245,106,0.4706)',
                            'rgba(255,245,106,0.12)'
                        ],
                        [
                            "eid7",
                            "background-color",
                            1000,
                            1000,
                            "linear",
                            "${Rectangle4}",
                            'rgba(255,245,106,0.12)',
                            'rgba(255,245,106,0.4706)'
                        ]
                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

    if (!window.edge_authoring_mode) AdobeEdge.getComposition(compId).load("index_edgeActions.js");
})("EDGE-261681945");
