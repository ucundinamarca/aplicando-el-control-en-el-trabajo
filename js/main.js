var ST = "#Stage_";
var S = "Stage_";
ivo.info({
    title: "Aplicando el control en el trabajo",
    autor: "Edilson Laverde Molina",
    date: "01/02/2021",
    email: "edilsonlaverde_182@hotmail.com",
    icon: 'https://www.ucundinamarca.edu.co/images/iconos/favicon_verde_1.png',
    facebook: "https://www.facebook.com/edilson.laverde.molina"
});
var audios=[
    {url:"sonidos/click.mp3",name:"click"},
    {url:"sonidos/Audio_1_Planeacion_Seguimiento_y_Control.mp3",name:"audio1"},
    {url:"sonidos/Audio_2.mp3",name:"audio2"},
    {url:"sonidos/Audio_3.mp3",name:"audio3"},
    {url:"sonidos/Audio_4.mp3",name:"audio4"},
    {url:"sonidos/Audio_5.mp3",name:"audio5"},
    {url:"sonidos/Audio_6.mp3",name:"audio6"},
    {url:"sonidos/Audio_7.mp3",name:"audio7"},
    {url:"sonidos/Audio_8.mp3",name:"audio8"}
];
var position=[];
var links=[
    {url:"https://www-ebooks7-24-com.ucundinamarca.basesdedatosezproxy.com/stage.aspx?il=5578&pg=&ed=",            id:"link1"},
    {url:"https://www.ucipfg.com/Repositorio/MLGA/MLGA-03/semana2/indicadores-de-gestion.pdf",                     id:"link2"},
    {url:"https://www.funcionpublica.gov.co/documents/418537/506911/1595.pdf/6c897f03-9b26-4e10-85a7-789c9e54f5a3",id:"link3"},
    {url:"https://www.redalyc.org/pdf/709/70930407002.pdf",                                                        id:"link4"}
];
var respuestas1 = [];
var respuestas2 = [];
var p1=0;p2=0;
var intento=1;
var soluccion1 = ["Stage_btn2", "Stage_btn6", "Stage_btn7"];
var soluccion2 = ["Stage_r1=Stage_g3", "Stage_r2=Stage_g1", "Stage_r3=Stage_g2"];
function main(sym) {
var items=[];
var udec = ivo.structure({
        created:function(){
           var t=this;
           ivo(ST+"Group").hide();
           ivo(ST+"Text12_").hide();
           ivo(ST+"reload1").hide();
           ivo(ST+"Group4").hide();
            //precarga audios//
           ivo.load_audio(audios,onComplete=function(){
                audio1.on('end', function(){
                    ivo.play("audio2") 
                });
                t.animation();
                t.events();
                view1.play();
                ivo(ST+"preload").hide();
           });
        },
        methods: {
            verifity:function(id){
                var open = false;
                for(x of respuestas1){
                    if(x==id){
                        return false;
                    }
                }
                if(open==false){
                    return true;
                }
            },
            push_element:function(id){
                var open = false;
                
                for(x of respuestas1){
                    if(x==id){
                        var i = respuestas1.indexOf( id );
                        respuestas1.splice( i, 1 );
                        open = true;
                    }
                }
                if(open==false){
                    if(respuestas1.length<3){
                        //retroalimentacion por cada respuesta
                        var good=false;
                        for(s of soluccion1){
                            if(s==id){
                                good=true;
                            }
                        }
                        if(good){
                            ivo(ST+"Text11").text("¡Correcto! Éste es un indicador de gestión aplicable al plan de acción de Leidy Duque.");
                        }else{
                            ivo(ST+"Text11").text("Incorrecto. Éste no es un indicador de gestión aplicable al plan de acción de Leidy Duque.");
                        }
                        respuestas1.push(id);
                        if(respuestas1.length==3){
                            let score=0;
                            var r1=respuestas1.find(el => el == soluccion1[0]);
                            var r2=respuestas1.find(el => el == soluccion1[1]);
                            var r3=respuestas1.find(el => el == soluccion1[2]);
                            if (r1 != undefined) {score+=1;}
                            if (r2 != undefined) {score+=1;}
                            if (r3 != undefined) {score+=1;}
                            ivo(ST+"Group").show();
                            ivo(ST+"Text4").text(score);
                            if(score==1){
                                ivo(ST+"Text5").text("Punto");
                            }else{
                                ivo(ST+"Text5").text("Puntos");
                            }
                            if(score==0){
                                ivo(ST+"Text5Copy").text("Algo ha fallado. Recuerde la importancia de consultar los recursos de apoyo sugeridos en torno al tema “Indicadores de gestión”");
                                if(intento==1){
                                    ivo(ST+"reload1").show();
                                }
                            }
                            if(score==1){
                                ivo(ST+"Text5Copy").text("¡Ánimo, siga intentándolo! Recuerde revisar los recursos de apoyo sugeridos en torno al tema “Indicadores de gestión”");
                                if(intento==1){
                                    ivo(ST+"reload1").show();
                                }
                            }
                            if(score==2){
                                ivo(ST+"Text5Copy").text("Va por buen camino, pero es necesario reforzar la lectura de los recursos sugeridos en torno al tema “Indicadores de gestión”");
                                if(intento==1){
                                    ivo(ST+"reload1").show();
                                }
                            }
                            if(score==3){
                                ivo(ST+"Text5Copy").text("¡Felicidades! Sin duda comprende a cabalidad qué es un indicador de gestión y su importancia a la hora de medir el alcance de la meta propuesta en un plan de acción.");
                            }
                            p1=score;
                            intento=2;
                            //console.log(score);
                        }
                    }
                }
            },
            events:function(){
                var t=this;
                ivo(ST+"tt").html(`
                    <div>
                        <p>
                            Una de las etapas más relevantes del proceso de control es la definición de las medidas de desempeño, también conocidas como <b class="r" id="r1">indicadores de gestión</b>, los cuales se definen como unidades de medida gerencial o administrativa que pretenden evaluar el desempeño de la organización frente a las metas, objetivos y actividades propuestas (Uribe y Reinoso, 2014, p. 13). Los indicadores de gestión pueden ser de naturaleza <b class="r" id="r2">cualitativa</b>  (por ejemplo, la obtención de una certificación en sistemas de gestión de calidad) o <b class="r" id="r3">cuantitativa </b>(por ejemplo, la tasa de productividad de un área específica de la organización), que pueden configurarse a través de razones, tasas, promedios, índices, entre otros.
                        </p>
                        <p>
                            En la siguiente actividad usted identificará cómo se aplican los indicadores de gestión en el área de trabajo mediante el análisis de un caso.
                        </p>
                    </div>
                `);
                ivo(ST+"reload1").attr("data-point","1");
                ivo(ST+"btn_iniciar").on("click",function(){
                    ivo.play("click");
                    view1.reverse().timeScale(3);
                    view2.play().timeScale(1);
                });
                ivo(ST+"close").on("click",function(){
                    ivo.play("click");
                    objetivo.reverse().timeScale(2);
                });
                ivo(ST+"close2").on("click",function(){
                    ivo.play("click");
                    intro.reverse().timeScale(2);
                });
                ivo(ST+"btn_objetivos").on("click",function(){
                    ivo.play("click");
                    objetivo.play().timeScale(1);
                });
                ivo(ST+"creditos").on("click",function(){
                    ivo.play("click");
                    creditos.reverse().timeScale(2);
                });
                ivo(ST+"btn_creditos").on("click",function(){
                    ivo.play("click");
                    creditos.play().timeScale(1);
                });
                ivo(ST+"Text7").on("click",function(){
                    ivo.play("click");
                    tablita.play().timeScale(1);
                });
                ivo(ST+"tablita").on("click",function(){
                    ivo.play("click");
                    tablita.reverse().timeScale(1);
                });
                ivo(".btn").on("click",function(){
                });
                ivo(".btn_aux").hide();
                ivo(".texto").css("color","#FFF");
                ivo(".btn").on("mouseover",function(){
                    var id=ivo(this).attr("id");
                    if(t.verifity(id)){
                        ivo("#"+id+" .btn_aux").show();
                        ivo("#"+id+" .texto").css("color","#000");
                    }
                }).on("mouseout",function(){
                    var id=ivo(this).attr("id");
                    if(t.verifity(id)){
                        ivo("#"+id+" .btn_aux").hide();
                        ivo("#"+id+" .texto").css("color","#FFF");
                    }
                }).on("click",function(){
                    t.push_element(ivo(this).attr("id"));
                });
                $( ".droppable" ).each(function() {
                    position.push({
                        top:$(this).css("top"),
                        left:$(this).css("left")
                    });
                });
                //slider principios//
                slider1= ivo(ST+"Group6").slider({
                    slides:'.contenido',
                    btn_next:ST+"Recurso_13",
                    btn_back:ST+"Recurso_12",
                    onMove:function(page){
                        ivo.play("click");
                        setTimeout(function(){
                            ivo("#r").css("font-weight","normal");
                            if(page==1){
                                ivo.play("audio3");
                            }
                            if(page==2){
                                ivo.play("audio4");
                                ivo("#r").css("font-weight","normal");
                            }
                            if(page==3){
                                ivo.play("audio5");
                               
                            }
                            if(page==4){
                                setTimeout(function(){
                                    ivo("#r").css("font-weight","bold");
                                },10000);
                                ivo.play("audio6");
                                
                            }
                        },1000)
                        if(page==4 || page==5){
                            caja.play();
                        }else{
                            caja.reverse();
                        }
                    },
                    onFinish:function(){
                    }
                });
                ivo(ST+"reload1").on("click",function(){
                    ivo(ST+"Text11").text("");
                    if(ivo(this).attr("data-point")=="1"){
                        respuestas1=[];
                        ivo(".btn_aux").hide();
                        ivo(this).hide();
                        ivo(".texto").css("color","#FFF");
                        ivo(ST+"Group").hide();
                    }
                    if(ivo(this).attr("data-point")=="2"){
                        ivo(ST+"Text12_").hide();
                        $(ST+"g1").animate({
                            "top": "0",
                            "left":"344px"
                        });
                        $(ST+"g2").animate({
                            "top": "112px",
                            "left":"344px"
                        });
                        $(ST+"g3").animate({
                            "top": "223px",
                            "left":"344px"
                        });
                        respuestas2=[];
                        ivo(ST+"legend").text("");
                        ivo(ST+"reload1").hide();
                    }
                });
                ivo(ST+"btn_sig").on("click",function(){
                    ivo(ST+"Group4").show();
                    ivo(ST+"Group2").hide();
                    ivo(ST+"Text11").text("");
                    intento=1;
                    slider1.go(5);
                    if(ivo(ST+"reload1").attr("data-point")=="1"){
                        ivo.play("audio7");
                    }
                    if(ivo(ST+"reload1").attr("data-point")=="2"){
                        
                        view4.play();
                        let t = p1 + p2;
                        let nota=0;
                        if(t==0 || t==1 || t==2){
                            ivo(ST+"Text10").html("Aún puede mejorar la identificación de los indicadores de gestión de acuerdo a la estructura de un plan de acción. Con una mayor revisión de los recursos educativos propuestos podrá lograr dicha meta. ¡Ánimo!.<br><br><br><h2>Nota: "+ ((t*50)/6).toFixed(2) +"</h2><h2>Puntos: "+t+"</h2>");
                        }
                        if(t==3 || t==4 || t==5 ){
                            ivo(ST+"Text10").html("¡Va por buen camino en cuanto a la identificación de los indicadores de gestión como herramientas para el seguimiento y control!<br><br><br><h2>Nota: "+((t*50)/6).toFixed(2)+"</h2><h2>Puntos: "+t+"</h2>");
                        }
                        if(t==6){
                            ivo(ST+"Text10").html("¡Felicitaciones! Ha identificado de manera satisfactoria los indicadores de gestión y su importancia en el monitoreo, seguimiento y control de un plan de acción.<br><br><br><h2>Nota: "+((t*50)/6).toFixed(2)+"</h2><h2>Puntos: "+t+"</h2>");
                        }
                        ivo.play("audio8");
                        Scorm_mx = new MX_SCORM(false);
                        console.log("Nombre: " + Scorm_mx.info_user().name + " Id: " + Scorm_mx.info_user().id);
                        Scorm_mx.set_score(((t*50)/6));
                    }
                });
                $( ".draggable" ).draggable({revert: true});
                $( ".droppable" ).droppable({
                    drop: function( event, ui ) {
                        let t = this;
                        $(ui.draggable).draggable( "option", "revert", "invalid" );
                        respuestas2.push($(this).attr("id")+"="+$(ui.draggable).attr("id"));
                        ui.draggable.animate( {
                            top : $(t).css("top"),
                            left: $(t).css("left")
                        });
                        let good=false;
                        for(var i=0;i<=2;i++){
                            if($(this).attr("id")+"="+$(ui.draggable).attr("id")==soluccion2[i]){
                                good=true;
                            }
                        }
                        if(good){
                            ivo(ST+"Text12").text("¡Correcto! Éste es un indicador de gestión aplicable al plan de acción de Leidy Duque. (Puntos 1)");
                        }else{
                            ivo(ST+"Text12").text("Incorrecto. Éste no es un indicador de gestión aplicable al plan de acción de Leidy Duque. (Puntos 0)");
                        }
                        ivo(ST+"Text12_").show();
                        if(respuestas2.length==3){
                            let score=0;
                            var r1=respuestas2.find(el => el == soluccion2[0]);
                            var r2=respuestas2.find(el => el == soluccion2[1]);
                            var r3=respuestas2.find(el => el == soluccion2[2]);
                            if (r1 != undefined) {score+=1;}
                            if (r2 != undefined) {score+=1;}
                            if (r3 != undefined) {score+=1;}
                            p2=score;
                            //console.log(score);
                            if(score==0){
                                ivo(ST+"legend").html("0 Puntos :  Algo ha fallado. Recuerde la importancia de consultar los recursos de apoyo sugeridos en torno al tema “Indicadores de gestión”");
                                if(intento==1){
                                    ivo(ST+"reload1").show();
                                }
                            }
                            if(score==1){
                                ivo(ST+"legend").html("1 Punto: ¡Ánimo, siga intentándolo! Recuerde revisar los recursos de apoyo sugeridos en torno al tema “Indicadores de gestión”");
                                if(intento==1){
                                    ivo(ST+"reload1").show();
                                }
                            }
                            if(score==2){
                                ivo(ST+"legend").html("2 Puntos: Va por buen camino, pero es necesario reforzar la lectura de los recursos sugeridos en torno al tema “Indicadores de gestión”");
                                if(intento==1){
                                    ivo(ST+"reload1").show();
                                }
                            }
                            if(score==3){
                                ivo(ST+"legend").html("3 Puntos: ¡Felicidades! Sin duda identifica el objeto de medición de un indicador de gestión.");
                            }
                            ivo(ST+"legend").hide();
                            
                            swal({
                                position: 'top-end',
                                title: ivo(ST+"legend").html(),
                                showConfirmButton: false,
                                timer: 15500
                            })
                            ivo(ST+"reload1").attr("data-point","2");
                            intento=2;
                        }
                    }
                  });
                for(l of links){
                    ivo(ST+l.id).attr("data-link",l.url);
                    ivo(ST+l.id).on("click",function(){
                        window.open(ivo(this).attr("data-link"),"_blank");
                    }).css("cursor","pointer");
                }
            },
            animation:function(){
                view1= new TimelineMax();
                view1.append(TweenMax.from(ST+"Recurso_2", .8,  {x:-1200,opacity:0}), 0);
                view1.append(TweenMax.from(ST+"Recurso_4", .8,  {x:1200, opacity:0}), 0);
                view1.append(TweenMax.from(ST+"btn_iniciar", .8,{x:1200, opacity:0}), 0);
                view1.stop();
                view2= new TimelineMax({onComplete:function(){
                    objetivo.play();
                }});
                view2.append(TweenMax.from(ST+"screen2", .5,    {x:-1200,opacity:0}), 0);
                view2.append(TweenMax.from(ST+"reloj", .5,      {x:-1200,opacity:0}), 0);
                view2.append(TweenMax.from(ST+"estante", .5,    {x:-1200,opacity:0}), 0);
                view2.append(TweenMax.from(ST+"matera", .5,     {x:-1200,opacity:0}), 0);
                view2.stop();
                view3= new TimelineMax({onStart:function(){
                    ivo.play("audio3");
                }});
                view3.append(TweenMax.from(ST+"screen3", .5,    {x:-1200,opacity:0}), 0);
                view3.stop();
                view4= new TimelineMax();
                view4.append(TweenMax.to(ST+"screen3", .5,    {x:-1200,opacity:0}), 0);
                view4.append(TweenMax.from(ST+"screen4", .5,    {x:-1200,opacity:0}), 0);
                view4.stop();
                tablita= new TimelineMax();
                tablita.append(TweenMax.from(ST+"tablita", .5,  {x:-1200,opacity:0}), 0);
                tablita.append(TweenMax.from(ST+"tabla", .5,    {x:-200,opacity:0}), 0);
                tablita.stop();
                objetivo= new TimelineMax({onReverseComplete:function(){
                    intro.play();
                }});
                objetivo.append(TweenMax.from(ST+"msg", .5,     {x:-1200,opacity:0}), 0);
                objetivo.append(TweenMax.from(ST+"close", .5,   {x:-1200,opacity:0}), 0);
                objetivo.stop();
                intro= new TimelineMax({
                    onStart:function(){
                        ivo.play("audio1");
                        setTimeout(function(){
                            ivo("#r1").removeClass("r");
                        },6000);
                        setTimeout(function(){
                            ivo("#r2").removeClass("r");
                        },21000);
                        setTimeout(function(){
                            ivo("#r3").removeClass("r");
                        },26000);
                    },onReverseComplete:function(){
                        view3.play();
                    }
                });
                intro.append(TweenMax.from(ST+"msg2", .5,     {x:-1200,opacity:0}), 0);
                intro.append(TweenMax.from(ST+"close2", .5,   {x:-1200,opacity:0}), 0);
                intro.stop();
                creditos= new TimelineMax();
                creditos.append(TweenMax.from(ST+"creditos", .5,{x:-1200,opacity:0}), 0);
                creditos.stop();
                caja= new TimelineMax();
                caja.append(TweenMax.to(ST+"caja", .5,          {x:-450}), 0);
                caja.append(TweenMax.to(ST+"avatar", .5,        {opacity:0}), 0);
                caja.append(TweenMax.from(ST+"indicadores", .5, {display:"none",x:200}), 0);
                caja.stop();
            }
        }
 });
}