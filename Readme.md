## Aplicando el control en el trabajo
> Identificar los indicadores de gestión que permiten evaluar y medir las metas establecidas en un plan de acción.


![Texto alternativo](/images/Recurso_16.png "Aplicando el control en el trabajo")
> Metadatos del programador

~~~~js
ivo.info
~~~~


> Inicializador disparador de eventos y animaciones 



~~~~js 
var udec = ivo.structure({
    created:function(){
        var t=this;
        ivo.load_audio(popup1,function(){
            ivo(ST+"preload").hide();
            scena1.play();
        });
        //llama los eventos para asociar lo oyentes
        this.events();
        //lama las animaciones para ponerlas en pila
        this.animation();
    }
}
~~~~

> Eventos
~~~~js 
events:function(){
    
},
~~~~

> Animaciones

~~~~js 
animation:function(){

}
~~~~